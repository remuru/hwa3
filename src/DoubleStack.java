import java.util.LinkedList;

public class DoubleStack {
    private LinkedList<Double> numbers;

    //NEEDS REWORK FOR PROPER ERROR HANDLING
    public static void main(String[] argum) {
        String s = "2 5 SWAP -";
        System.out.println(DoubleStack.interpret(s));
        String s2 = "2 5 9 ROT - +";
        System.out.println(DoubleStack.interpret(s2));
        String s3 = "2 5 9 ROT + SWAP -";
        System.out.println(DoubleStack.interpret(s3));
        //String s4 = "2 5  ROT + SWAP -";
        //System.out.println(DoubleStack.interpret(s4));
        //String s5 = "2  SWAP -";
        //System.out.println(DoubleStack.interpret(s5));

    }

    DoubleStack() {
        numbers = new LinkedList<>();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        DoubleStack clone = new DoubleStack();
        for (Double aDouble : numbers) {
            clone.push(aDouble);
        }
        return clone;
    }

    public boolean stEmpty() {
        return numbers.size() == 0;
    }

    public void push(double a) {
        numbers.add(a);
    }

    public int size() {
        return numbers.size();
    }


    public double pop() {
        if (stEmpty())
            throw new IndexOutOfBoundsException("Stack is empty!");
        return numbers.removeLast();
    } // pop

    //Arithmetic operation <code>s</code> ( <code>+ - * /</code> ) between two
//topmost elements of the stack (result is left on top):
    public void op(String s) throws RuntimeException {
        if (!stEmpty()) {
            if (numbers.size() < 2) {
                throw new RuntimeException("Not enough arguments in stack: " + numbers.toString() +
                        " to do operation " + s);
            }
            double last = pop();
            double second = pop();
            if (s.equals("+"))
                push(second + last);
            else if (s.equals("-"))
                push(second - last);
            else if (s.equals("/"))
                push(second / last);
            else if (s.equals("*"))
                push(second * last);
        } else {
            throw new RuntimeException("Stack is empty: " + numbers.toString());
        }

    }

    public double tos() {
        if (stEmpty())
            throw new IndexOutOfBoundsException("Nothing to return!");
        return numbers.getLast();
    }

    @Override
    public boolean equals(Object o) {
        return numbers.equals(((DoubleStack) o).numbers);
    }

    @Override
    public String toString() {
        return numbers.toString();
    }

    //One possible solution from https://codereview.stackexchange.com/questions/120451/reverse-polish-notation-calculator-in-java
    public static double interpret(String input) throws RuntimeException {
        // Reverse Polish Notation
        DoubleStack numbers = new DoubleStack();
        input = input.replaceAll("\\s+", " ").trim();
        int i = 0, o = 0;
        if (input.isEmpty()) {
            throw new RuntimeException("Input is empty!");
        }
        for (String token : input.split(" ")) {
            try {
                numbers.push(Double.parseDouble(token));
                i++;
            } catch (Exception e) {
                switch (token) {
                    case "SWAP":
                        if (
                                numbers.size() < 2) {
                            throw new RuntimeException("Not enough numeric arguments for SWAP in expression " + input + " !");
                        }
                        double first = numbers.pop();
                        double second = numbers.pop();
                        numbers.push(first);
                        numbers.push(second);
                        break;
                    case "ROT":
                        if (
                                numbers.size() < 3) {
                            throw new RuntimeException("Not enough numeric arguments for ROT in expression " + input + " !");
                        }
                        double secondrot = numbers.pop();
                        double firstrot = numbers.pop();
                        double thirdrot = numbers.pop();
                        numbers.push(firstrot);
                        numbers.push(secondrot);
                        numbers.push(thirdrot);
                        break;
                    case "+":
                        if (
                                numbers.size() < 2) {
                            throw new RuntimeException("Not enough numeric arguments for '+' in expression " + input + " !");
                        }
                        numbers.push(numbers.pop() + numbers.pop());
                        o++;
                        break;
                    case "-":
                        if (
                                numbers.size() < 2) {
                            throw new RuntimeException("Not enough numeric arguments for '-' in expression " + input + " !");
                        }
                        numbers.push(-numbers.pop() + numbers.pop());
                        o++;
                        break;
                    case "*":
                        if (
                                numbers.size() < 2) {
                            throw new RuntimeException("Not enough numeric arguments for '*' in expression " + input + " !");
                        }
                        numbers.push(numbers.pop() * numbers.pop());
                        o++;
                        break;
                    case "/":
                        if (
                                numbers.size() < 2) {
                            throw new RuntimeException("Not enough numeric arguments for '/' in expression " + input + " !");
                        }
                        double divisor = numbers.pop();
                        numbers.push(numbers.pop() / divisor);
                        o++;
                        break;
                    default:
                        throw new RuntimeException("Unknown symbol " + token + " in input " + input + " !");
                }
            }

        }
        if (i - 1 != o) {
            throw new RuntimeException("There are too many numbers in input " + input);
        }
        return numbers.pop();
    }
}
